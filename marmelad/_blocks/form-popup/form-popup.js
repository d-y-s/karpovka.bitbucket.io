$('.js-popup-form .form-popup__form').on('submit', function (event) {
    var $this = $(this);
    event.preventDefault();
    $this.closest('.form-popup__form').addClass('success');
    $this.closest('.js-popup-form').find('.form-popup__send-success').addClass('success');
    setTimeout(function () {
        $this.closest('.app').find('.remodal-overlay').css('display', 'none');
        $this.closest('.app').find('.remodal-wrapper').css('display', 'none');
        $this.closest('.form-popup__form').removeClass('success');
        $this.closest('.js-popup-form').find('.form-popup__send-success').removeClass('success');
        $('html').removeClass('remodal-is-locked');
        $('body').css('padding-right', 0);
    }, 3000);
});