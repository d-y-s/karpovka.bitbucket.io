var videoModal = $('[data-remodal-id=video]').remodal();
$('.video-content__btn-play').on('click', function (event) {
    event.preventDefault();
    var videoSrc = $(this).attr('href');
    $('[data-remodal-id="video"] iframe').attr('src', videoSrc);
    videoModal.open();
});
$(document).on('closed', '[data-remodal-id=video]', function (e) {
    $('[data-remodal-id="video"] iframe').attr('src', '');
});