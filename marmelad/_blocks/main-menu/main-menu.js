$('.js-main-menu-btn').on('click', function () {
    $(this).toggleClass('is-active');
    $('.js-main-menu').fadeToggle();

    if (!$(this).hasClass('.is-active')) {
        $('.main-menu__nav > ul .sub-menu ul li').removeClass('is-active-specialists');
        $('.main-menu-specialists-info').hide();
    }
});

// $('.sub-menu li a').not('.sub-menu.our-specialists-list li a').each(function () {
//     $(this).hover(
//         function () {
//             $('.main-menu-info').stop().fadeIn();
//         },
//         function () {
//             $('.main-menu-info').stop().fadeOut();
//         });
// });

$('.our-specialists-list li').on('click', function (event) {
    event.preventDefault();
    $(this).addClass('is-active-specialists').siblings().removeClass('is-active-specialists');
    $(this).find('.main-menu-specialists-info').stop().fadeIn()
        .closest($(this))
        .siblings()
        .find('.main-menu-specialists-info').fadeOut();
    
    // if (window.matchMedia("(max-width: 1279px)").matches) {

    // var btn = $('.our-specialists-list li');

    // btn.on('click', function (e) {
    //     e.preventDefault();
    //     $('.main-menu__nav > ul .sub-menu ul').animate({ scrollTop: 0 }, '300');
    // });
    // }
});

// $('.main-menu-specialists-info').on('mousemove', function () {
//     $('.our-specialists-list ul').addClass('show-list');
// })

// $('.main-menu-specialists-info').on('mouseleave', function () {
//     $('.our-specialists-list ul').removeClass('show-list');
//     $(this).stop().fadeOut();
//     $('.our-specialists-list li').removeClass('is-active-specialists');
// });

// $('.main-menu__nav > ul').on('mousemove', function () {
//     $('.main-menu-specialists-info').fadeOut();
//     $('.our-specialists-list li').removeClass('is-active-specialists');
// });

$(document).on('click', function (e) {
    if ($(e.target).closest('.app-header__inner').length) {
        return;
    }

    $('.js-main-menu-btn').removeClass('is-active');
    $('.js-main-menu').fadeOut();
    $('.our-specialists-list ul').removeClass('show-list');
    $('.main-menu-specialists-info').hide();
    $('.our-specialists-list li').removeClass('is-active-specialists');
});


