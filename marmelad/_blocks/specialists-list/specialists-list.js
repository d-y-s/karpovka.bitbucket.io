if (window.matchMedia('(max-width:1024px)').matches) {
	$('.specialists-list__filter-title').on('click', function(e){
		e.preventDefault();
	    $(this).siblings().stop().slideToggle();
	});
	$(document).on('click', function(e){
	  if ($(e.target).closest('.specialists-list__filter').length) {
	    return;
	  }

		$('.specialists-list__filter-body').slideUp();
	});
}
