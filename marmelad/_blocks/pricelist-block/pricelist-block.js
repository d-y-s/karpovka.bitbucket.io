$('.pricelist-block__categor > ul > li > a').on('click', function(e){
	e.preventDefault();

	var $this = $(this),
		wrapper = $this.closest('.pricelist-block__list-wr'),
		ind = $this.closest('li').index();

	wrapper.find('.pricelist-block__pricelist-body, .pricelist-block__categor > ul > li').removeClass('active');
	$this.closest('li').addClass('active');
	wrapper.find('.pricelist-block__pricelist-body:eq(' + ind + ')').addClass('active');

	if (window.matchMedia('(max-width:766px)').matches) {
        $('html, body').animate({
            scrollTop: ($('.pricelist-block__column.pricelist-block__column--right').offset().top - 70)
        }, 600, function() {
        });
	}
});
