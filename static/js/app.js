"use strict";

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/**
  * Возвращает HTML-код иконки из SVG-спрайта
  *
  * @param {String} name Название иконки из спрайта
  * @param {Object} opts Объект настроек для SVG-иконки
  *
  * @example SVG-иконка
  * getSVGSpriteIcon('some-icon', {
  *   tag: 'div',
  *   type: 'icons', // colored для подключения иконки из цветного спрайта
  *   class: '', // дополнительные классы для иконки
  *   mode: 'inline', // external для подключаемых спрайтов
  *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
  * });
  */
function getSVGSpriteIcon(name, opts) {
  opts = _extends({
    tag: 'div',
    type: 'icons',
    "class": '',
    mode: 'inline',
    url: ''
  }, opts);
  var external = '';
  var typeClass = '';

  if (opts.mode === 'external') {
    external = "".concat(opts.url, "/sprite.").concat(opts.type, ".svg");
  }

  if (opts.type !== 'icons') {
    typeClass = " svg-icon--".concat(opts.type);
  }

  opts["class"] = opts["class"] ? " ".concat(opts["class"]) : '';
  return "\n    <".concat(opts.tag, " class=\"svg-icon svg-icon--").concat(name).concat(typeClass).concat(opts["class"], "\" aria-hidden=\"true\" focusable=\"false\">\n      <svg class=\"svg-icon__link\">\n        <use xlink:href=\"").concat(external, "#").concat(name, "\"></use>\n      </svg>\n    </").concat(opts.tag, ">\n  ");
}
/* ^^^
 * JQUERY Actions
 * ========================================================================== */


$(function () {
  'use strict';
  /**
   * определение существования элемента на странице
   */

  $.exists = function (selector) {
    return $(selector).length > 0;
  };

  $('.js-toggle-acc').on('click', function () {
    $(this).toggleClass('is-active');
    $(this).siblings().stop().slideToggle();
  });
  $.each($('.categories__nav ul').find('> li'), function (index, element) {
    if ($(element).find(' > ul').length) {
      var triggerIcon = ['<div class="svg-icon svg-icon--angle-down">', '</div>'].join('');
      var subMenuTrigger = $('<div class="sub-menu-trigger">' + triggerIcon + '</div>');
      $(element).addClass('haschild').append(subMenuTrigger);
    }
  });
  $('.sub-menu-trigger').on('click', function (event) {
    $(this).toggleClass('rotade');

    if (!$(this).closest('li').find('>ul').length) {
      return;
    }

    event.preventDefault();
    $(this).closest('li').toggleClass('open').find('>ul').stop().slideToggle();
  });
  $('.js-popup-form .form-popup__form').on('submit', function (event) {
    var $this = $(this);
    event.preventDefault();
    $this.closest('.form-popup__form').addClass('success');
    $this.closest('.js-popup-form').find('.form-popup__send-success').addClass('success');
    setTimeout(function () {
      $this.closest('.app').find('.remodal-overlay').css('display', 'none');
      $this.closest('.app').find('.remodal-wrapper').css('display', 'none');
      $this.closest('.form-popup__form').removeClass('success');
      $this.closest('.js-popup-form').find('.form-popup__send-success').removeClass('success');
      $('html').removeClass('remodal-is-locked');
      $('body').css('padding-right', 0);
    }, 3000);
  });
  $('.js-main-menu-btn').on('click', function () {
    $(this).toggleClass('is-active');
    $('.js-main-menu').fadeToggle();

    if (!$(this).hasClass('.is-active')) {
      $('.main-menu__nav > ul .sub-menu ul li').removeClass('is-active-specialists');
      $('.main-menu-specialists-info').hide();
    }
  }); // $('.sub-menu li a').not('.sub-menu.our-specialists-list li a').each(function () {
  //     $(this).hover(
  //         function () {
  //             $('.main-menu-info').stop().fadeIn();
  //         },
  //         function () {
  //             $('.main-menu-info').stop().fadeOut();
  //         });
  // });

  $('.our-specialists-list li').on('click', function (event) {
    event.preventDefault();
    $(this).addClass('is-active-specialists').siblings().removeClass('is-active-specialists');
    $(this).find('.main-menu-specialists-info').stop().fadeIn().closest($(this)).siblings().find('.main-menu-specialists-info').fadeOut(); // if (window.matchMedia("(max-width: 1279px)").matches) {
    // var btn = $('.our-specialists-list li');
    // btn.on('click', function (e) {
    //     e.preventDefault();
    //     $('.main-menu__nav > ul .sub-menu ul').animate({ scrollTop: 0 }, '300');
    // });
    // }
  }); // $('.main-menu-specialists-info').on('mousemove', function () {
  //     $('.our-specialists-list ul').addClass('show-list');
  // })
  // $('.main-menu-specialists-info').on('mouseleave', function () {
  //     $('.our-specialists-list ul').removeClass('show-list');
  //     $(this).stop().fadeOut();
  //     $('.our-specialists-list li').removeClass('is-active-specialists');
  // });
  // $('.main-menu__nav > ul').on('mousemove', function () {
  //     $('.main-menu-specialists-info').fadeOut();
  //     $('.our-specialists-list li').removeClass('is-active-specialists');
  // });

  $(document).on('click', function (e) {
    if ($(e.target).closest('.app-header__inner').length) {
      return;
    }

    $('.js-main-menu-btn').removeClass('is-active');
    $('.js-main-menu').fadeOut();
    $('.our-specialists-list ul').removeClass('show-list');
    $('.main-menu-specialists-info').hide();
    $('.our-specialists-list li').removeClass('is-active-specialists');
  });
  $('.js-slider-main').owlCarousel({
    items: 1,
    loop: true,
    nav: false,
    dots: true,
    mouseDrag: false,
    animateOut: 'fadeOut',
    animateIn: 'slideOutin'
  });
  $('.js-btn-burger').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('btn-burger-is-active');
    $('.mobile-panel .menu-mob').toggleClass('menu-is-active');
    $('.mobile-panel__overlay-menu').toggleClass('overlay-is-active');
  });
  $.each($('.main-menu-mob__nav-list').find('> li'), function (index, element) {
    if ($(element).find(' > ul').length) {
      var triggerIcon = ['<div class="svg-icon svg-icon--angle-down">', '</div>'].join('');
      var subMenuTrigger = $('<div class="sub-menu-trigger">' + triggerIcon + '</div>');
      $(element).addClass('haschild').append(subMenuTrigger);
    }
  });
  $('.main-menu-mob__nav-list .sub-menu-trigger').on('click', function (event) {
    $(this).toggleClass('rotade');

    if (!$(this).closest('li').find('>ul').length) {
      return;
    }

    event.preventDefault();
    $(this).closest('li').toggleClass('open').find('>ul').stop().slideToggle();
  });

  function btnPhone() {
    $('.js-btn-phone').on('click', function () {
      $('.js-contacts-body').fadeToggle();
    });
    $(document).on('click', function (e) {
      if ($(e.target).closest('.mobile-panel').length) {
        return;
      }

      $('.js-contacts-body').fadeOut();
    });
  }

  btnPhone();
  $('.js-slider-news').owlCarousel({
    items: 3,
    loop: true,
    nav: true,
    dots: false,
    navText: ['<svg xmlns="http://www.w3.org/2000/svg" width="15" height="7" viewBox="0 0 15 7"><g><g><path fill="#d7dde1" d="M.272.235c.378-.311.99-.311 1.367 0l5.843 4.804L13.324.235c.378-.311.99-.311 1.368 0 .377.31.377.813 0 1.124l-6.202 5.1a.779.779 0 0 1-.243.333c-.21.173-.491.25-.765.23a1.093 1.093 0 0 1-.765-.23.779.779 0 0 1-.243-.333l-6.202-5.1c-.377-.31-.377-.814 0-1.124z"/></g></g></svg>', '<svg xmlns="http://www.w3.org/2000/svg" width="15" height="7" viewBox="0 0 15 7"><g><g><path fill="#d7dde1" d="M.272.235c.378-.311.99-.311 1.367 0l5.843 4.804L13.324.235c.378-.311.99-.311 1.368 0 .377.31.377.813 0 1.124l-6.202 5.1a.779.779 0 0 1-.243.333c-.21.173-.491.25-.765.23a1.093 1.093 0 0 1-.765-.23.779.779 0 0 1-.243-.333l-6.202-5.1c-.377-.31-.377-.814 0-1.124z"/></g></g></svg>'],
    responsive: {
      320: {
        items: 1
      },
      768: {
        items: 2
      }
    }
  });
  $('.pricelist-block__categor > ul > li > a').on('click', function (e) {
    e.preventDefault();
    var $this = $(this),
        wrapper = $this.closest('.pricelist-block__list-wr'),
        ind = $this.closest('li').index();
    wrapper.find('.pricelist-block__pricelist-body, .pricelist-block__categor > ul > li').removeClass('active');
    $this.closest('li').addClass('active');
    wrapper.find('.pricelist-block__pricelist-body:eq(' + ind + ')').addClass('active');

    if (window.matchMedia('(max-width:766px)').matches) {
      $('html, body').animate({
        scrollTop: $('.pricelist-block__column.pricelist-block__column--right').offset().top - 70
      }, 600, function () {});
    }
  });
  $('.js-slider-reviews').owlCarousel({
    items: 4,
    loop: true,
    nav: true,
    dots: false,
    navText: ['<svg xmlns="http://www.w3.org/2000/svg" width="15" height="7" viewBox="0 0 15 7"><g><g><path fill="#d7dde1" d="M.272.235c.378-.311.99-.311 1.367 0l5.843 4.804L13.324.235c.378-.311.99-.311 1.368 0 .377.31.377.813 0 1.124l-6.202 5.1a.779.779 0 0 1-.243.333c-.21.173-.491.25-.765.23a1.093 1.093 0 0 1-.765-.23.779.779 0 0 1-.243-.333l-6.202-5.1c-.377-.31-.377-.814 0-1.124z"/></g></g></svg>', '<svg xmlns="http://www.w3.org/2000/svg" width="15" height="7" viewBox="0 0 15 7"><g><g><path fill="#d7dde1" d="M.272.235c.378-.311.99-.311 1.367 0l5.843 4.804L13.324.235c.378-.311.99-.311 1.368 0 .377.31.377.813 0 1.124l-6.202 5.1a.779.779 0 0 1-.243.333c-.21.173-.491.25-.765.23a1.093 1.093 0 0 1-.765-.23.779.779 0 0 1-.243-.333l-6.202-5.1c-.377-.31-.377-.814 0-1.124z"/></g></g></svg>'],
    responsive: {
      320: {
        items: 1
      },
      768: {
        items: 2
      },
      960: {
        items: 3
      },
      1024: {
        items: 4
      }
    }
  });
  $('.js-slider-specialists').owlCarousel({
    items: 4,
    loop: true,
    nav: true,
    dots: false,
    navText: ['<svg xmlns="http://www.w3.org/2000/svg" width="15" height="7" viewBox="0 0 15 7"><g><g><path fill="#d7dde1" d="M.272.235c.378-.311.99-.311 1.367 0l5.843 4.804L13.324.235c.378-.311.99-.311 1.368 0 .377.31.377.813 0 1.124l-6.202 5.1a.779.779 0 0 1-.243.333c-.21.173-.491.25-.765.23a1.093 1.093 0 0 1-.765-.23.779.779 0 0 1-.243-.333l-6.202-5.1c-.377-.31-.377-.814 0-1.124z"/></g></g></svg>', '<svg xmlns="http://www.w3.org/2000/svg" width="15" height="7" viewBox="0 0 15 7"><g><g><path fill="#d7dde1" d="M.272.235c.378-.311.99-.311 1.367 0l5.843 4.804L13.324.235c.378-.311.99-.311 1.368 0 .377.31.377.813 0 1.124l-6.202 5.1a.779.779 0 0 1-.243.333c-.21.173-.491.25-.765.23a1.093 1.093 0 0 1-.765-.23.779.779 0 0 1-.243-.333l-6.202-5.1c-.377-.31-.377-.814 0-1.124z"/></g></g></svg>'],
    responsive: {
      320: {
        items: 1
      },
      768: {
        items: 2
      },
      960: {
        items: 3
      },
      1024: {
        items: 4
      }
    }
  });

  if (window.matchMedia('(max-width:1024px)').matches) {
    $('.specialists-list__filter-title').on('click', function (e) {
      e.preventDefault();
      $(this).siblings().stop().slideToggle();
    });
    $(document).on('click', function (e) {
      if ($(e.target).closest('.specialists-list__filter').length) {
        return;
      }

      $('.specialists-list__filter-body').slideUp();
    });
  }

  $('.js-slider-type-service').owlCarousel({
    items: 1,
    loop: true,
    nav: false,
    dots: true,
    mouseDrag: false,
    animateOut: 'fadeOut',
    animateIn: 'slideOutin'
  });
  var videoModal = $('[data-remodal-id=video]').remodal();
  $('.video-content__btn-play').on('click', function (event) {
    event.preventDefault();
    var videoSrc = $(this).attr('href');
    $('[data-remodal-id="video"] iframe').attr('src', videoSrc);
    videoModal.open();
  });
  $(document).on('closed', '[data-remodal-id=video]', function (e) {
    $('[data-remodal-id="video"] iframe').attr('src', '');
  });
}); // contacts map

var center1 = {
  lat: 59.9702528,
  lng: 30.3090334
};
var center2 = {
  lat: 59.9518887,
  lng: 30.2968963
};

function initStandortMap() {
  var standortMap = document.getElementById('map');

  if (!standortMap) {
    return;
  }

  var map = new google.maps.Map(standortMap, {
    center: {
      lat: center1.lat,
      lng: center1.lng
    },
    disableDefaultUI: true,
    zoom: 15,
    styles: [{
      "featureType": "all",
      "elementType": "geometry",
      "stylers": [{
        "color": "#f5f5f5"
      }, {
        "visibility": "on"
      }]
    }, {
      "featureType": "all",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#616161"
      }]
    }, {
      "featureType": "all",
      "elementType": "labels.text.stroke",
      "stylers": [{
        "color": "#f5f5f5"
      }]
    }, {
      "featureType": "all",
      "elementType": "labels.icon",
      "stylers": [{
        "visibility": "on"
      }, {
        "color": "#bfbfbf"
      }]
    }, {
      "featureType": "administrative.land_parcel",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#bdbdbd"
      }]
    }, {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [{
        "color": "#eeeeee"
      }]
    }, {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#757575"
      }]
    }, {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [{
        "color": "#e5e5e5"
      }]
    }, {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#9e9e9e"
      }]
    }, {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [{
        "color": "#ffffff"
      }]
    }, {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [{
        "color": "#dadada"
      }]
    }, {
      "featureType": "road.highway",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#616161"
      }]
    }, {
      "featureType": "road.arterial",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#757575"
      }]
    }, {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#9e9e9e"
      }]
    }, {
      "featureType": "transit.line",
      "elementType": "geometry",
      "stylers": [{
        "color": "#e5e5e5"
      }]
    }, {
      "featureType": "transit.station",
      "elementType": "geometry",
      "stylers": [{
        "color": "#eeeeee"
      }]
    }, {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [{
        "color": "#c9c9c9"
      }]
    }, {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#9e9e9e"
      }]
    }]
  });
  var marker2 = new google.maps.Marker({
    position: {
      lat: center1.lat,
      lng: center1.lng
    },
    map: map,
    icon: 'img/map-icon.png'
  });

  function newLocation(newLat, newLng) {
    map.setCenter({
      lat: newLat,
      lng: newLng
    });
  }

  $("#other_loc").on('click', function () {
    newLocation(59.9702528, 30.3090334);
    var marker2 = new google.maps.Marker({
      position: {
        lat: 59.9702528,
        lng: 30.3090334
      },
      map: map,
      icon: 'img/map-icon.png'
    });
    $(this).closest('.contacts-info__body').fadeOut();
    $(this).closest('.contacts-info__body').siblings('.contacts-info__body').fadeIn(800);
  });
  $("#main_loc").on('click', function () {
    newLocation(59.9518887, 30.2968963);
    var marker2 = new google.maps.Marker({
      position: {
        lat: 59.9518887,
        lng: 30.2968963
      },
      map: map,
      icon: 'img/map-icon.png'
    });
    $(this).closest('.contacts-info__body').fadeOut();
    $(this).closest('.contacts-info__body').siblings('.contacts-info__body').fadeIn(800);
  });
} // contacts-page map1


var centerContactsPage1 = {
  lat: 59.9702528,
  lng: 30.3090334
};

function initContactsPage1() {
  var ContactsPage1 = document.getElementById('map_central');

  if (!ContactsPage1) {
    return;
  }

  var map = new google.maps.Map(ContactsPage1, {
    center: {
      lat: centerContactsPage1.lat,
      lng: centerContactsPage1.lng
    },
    disableDefaultUI: true,
    zoom: 15,
    styles: [{
      "featureType": "all",
      "elementType": "geometry",
      "stylers": [{
        "color": "#f5f5f5"
      }, {
        "visibility": "on"
      }]
    }, {
      "featureType": "all",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#616161"
      }]
    }, {
      "featureType": "all",
      "elementType": "labels.text.stroke",
      "stylers": [{
        "color": "#f5f5f5"
      }]
    }, {
      "featureType": "all",
      "elementType": "labels.icon",
      "stylers": [{
        "visibility": "on"
      }, {
        "color": "#bfbfbf"
      }]
    }, {
      "featureType": "administrative.land_parcel",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#bdbdbd"
      }]
    }, {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [{
        "color": "#eeeeee"
      }]
    }, {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#757575"
      }]
    }, {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [{
        "color": "#e5e5e5"
      }]
    }, {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#9e9e9e"
      }]
    }, {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [{
        "color": "#ffffff"
      }]
    }, {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [{
        "color": "#dadada"
      }]
    }, {
      "featureType": "road.highway",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#616161"
      }]
    }, {
      "featureType": "road.arterial",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#757575"
      }]
    }, {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#9e9e9e"
      }]
    }, {
      "featureType": "transit.line",
      "elementType": "geometry",
      "stylers": [{
        "color": "#e5e5e5"
      }]
    }, {
      "featureType": "transit.station",
      "elementType": "geometry",
      "stylers": [{
        "color": "#eeeeee"
      }]
    }, {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [{
        "color": "#c9c9c9"
      }]
    }, {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#9e9e9e"
      }]
    }]
  });
  var marker2 = new google.maps.Marker({
    position: {
      lat: centerContactsPage1.lat,
      lng: centerContactsPage1.lng
    },
    map: map,
    icon: 'img/map-icon.png'
  });
} // contacts-page map2


var centerContactsPage2 = {
  lat: 59.9518887,
  lng: 30.2968963
};

function initContactsPage2() {
  var ContactsPage2 = document.getElementById('map_branch');

  if (!ContactsPage2) {
    return;
  }

  var map = new google.maps.Map(ContactsPage2, {
    center: {
      lat: centerContactsPage2.lat,
      lng: centerContactsPage2.lng
    },
    disableDefaultUI: true,
    zoom: 15,
    styles: [{
      "featureType": "all",
      "elementType": "geometry",
      "stylers": [{
        "color": "#f5f5f5"
      }, {
        "visibility": "on"
      }]
    }, {
      "featureType": "all",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#616161"
      }]
    }, {
      "featureType": "all",
      "elementType": "labels.text.stroke",
      "stylers": [{
        "color": "#f5f5f5"
      }]
    }, {
      "featureType": "all",
      "elementType": "labels.icon",
      "stylers": [{
        "visibility": "on"
      }, {
        "color": "#bfbfbf"
      }]
    }, {
      "featureType": "administrative.land_parcel",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#bdbdbd"
      }]
    }, {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [{
        "color": "#eeeeee"
      }]
    }, {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#757575"
      }]
    }, {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [{
        "color": "#e5e5e5"
      }]
    }, {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#9e9e9e"
      }]
    }, {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [{
        "color": "#ffffff"
      }]
    }, {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [{
        "color": "#dadada"
      }]
    }, {
      "featureType": "road.highway",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#616161"
      }]
    }, {
      "featureType": "road.arterial",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#757575"
      }]
    }, {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#9e9e9e"
      }]
    }, {
      "featureType": "transit.line",
      "elementType": "geometry",
      "stylers": [{
        "color": "#e5e5e5"
      }]
    }, {
      "featureType": "transit.station",
      "elementType": "geometry",
      "stylers": [{
        "color": "#eeeeee"
      }]
    }, {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [{
        "color": "#c9c9c9"
      }]
    }, {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#9e9e9e"
      }]
    }]
  });
  var marker2 = new google.maps.Marker({
    position: {
      lat: centerContactsPage2.lat,
      lng: centerContactsPage2.lng
    },
    map: map,
    icon: 'img/map-icon.png'
  });
}

function initMap() {
  initStandortMap();
  initContactsPage1();
  initContactsPage2();
  $('.js-other-loc').on('click', function (event) {
    event.preventDefault();
  });
}

if (window.matchMedia("(min-width: 1025px)").matches) {
  var fixedPanel = $('.js-fixed-panel');
  $(window).scroll(function () {
    if ($(this).scrollTop() > 200 && fixedPanel.hasClass('fixed-panel-init')) {
      fixedPanel.addClass('fixed-panel-show');
      $('.app-header').css('padding-top', '180px');
    } else if ($(this).scrollTop() <= 250 && fixedPanel.hasClass('fixed-panel-init')) {
      fixedPanel.removeClass('fixed-panel-show');
      $('.app-header').css('padding-top', '0');
    }
  });
}